<?php

namespace Hurricane\FacadeBundle\Tests;

use Hurricane\FacadeBundle\AbstractFacade;
use Hurricane\FacadeBundle\Tests\Fixtures\Facades\Foo;
use Hurricane\FacadeBundle\Tests\Fixtures\Services\FooService;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\ServiceLocator;

class FacadeTest extends TestCase
{
    public function testRegisteredFacade(): void
    {
        $container = $this->getMockBuilder(ServiceLocator::class)
            ->disableOriginalConstructor()
            ->getMock();
        $container
            ->expects($this->exactly(2))
            ->method('has')
            ->will($this->returnValueMap([
                [Foo::class, true],
            ]))
        ;
        $container
            ->expects($this->exactly(2))
            ->method('get')
            ->will($this->returnValueMap([
                [Foo::class, new FooService()],
            ]))
        ;
        AbstractFacade::setFacadeContainer($container);
        $fooService = new FooService();
        $this->assertSame($fooService->sayHello(), Foo::sayHello());
        $this->assertSame($fooService->callWithArgs('foo', 'bar'), Foo::callWithArgs('foo', 'bar'));
    }
    public function testNotRegisteredFacade(): void
    {
        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage(sprintf('"%s" facade has not been register.', Foo::class));
        $container = $this->createMock(ServiceLocator::class);
        $container
            ->expects($this->once())
            ->method('has')
            ->will($this->returnValueMap([]))
        ;
        AbstractFacade::setFacadeContainer($container);
        $fooService = new FooService();
        Foo::sayHello();
    }
}
