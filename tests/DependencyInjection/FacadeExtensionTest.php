<?php

namespace Hurricane\FacadeBundle\Tests\DependencyInjection;

use Hurricane\FacadeBundle\AbstractFacade;
use Hurricane\FacadeBundle\DependencyInjection\FacadeExtension;
use Hurricane\FacadeBundle\Tests\Fixtures\Facades\Foo;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class FacadeExtensionTest extends TestCase
{
    public function testRegisterAutoConfigure(): void
    {
        $container = new ContainerBuilder();
        $container->register(Foo::class);
        $extension = new FacadeExtension();
        $extension->load([], $container);
        $this->assertArrayHasKey(AbstractFacade::class, $container->getAutoconfiguredInstanceof());
        $this->assertArrayHasKey(
            'hurricane.facade',
            $container->getAutoconfiguredInstanceof()[AbstractFacade::class]->getTags()
        );
    }
}
