<?php

namespace Hurricane\FacadeBundle\Tests\DependencyInjection\Compiler;

use Hurricane\FacadeBundle\DependencyInjection\Compiler\AddFacadePass;
use Hurricane\FacadeBundle\Tests\Fixtures\Facades\Foo;
use Hurricane\FacadeBundle\Tests\Fixtures\Facades\InvalidFacade;
use Hurricane\FacadeBundle\Tests\Fixtures\Services\FooService;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\ServiceLocator;

class AddFacadePassTest extends TestCase
{
    public function testProsesFacade()
    {
        $container = new ContainerBuilder();
        $container->register(FooService::class);
        $container->register(Foo::class)->addTag('hurricane.facade');
        $addFacadePass = new AddFacadePass();
        $addFacadePass->process($container);
        $container->compile();
        $this->assertInstanceOf(ServiceLocator::class, $container->get('hurricane.facade.container'));
        $this->assertTrue($container->get('hurricane.facade.container')->has(Foo::class));
    }

    public function testProsesInvalidFacade()
    {
        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage(
            'The service "Hurricane\FacadeBundle\Tests\Fixtures\Facades\InvalidFacade" must extend AbstractFacade.'
        );
        $container = new ContainerBuilder();
        $container->register(InvalidFacade::class)->addTag('hurricane.facade');
        $addFacadePass = new AddFacadePass();
        $addFacadePass->process($container);
        $container->compile();
    }
}
