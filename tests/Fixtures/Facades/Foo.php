<?php

namespace Hurricane\FacadeBundle\Tests\Fixtures\Facades;

use Hurricane\FacadeBundle\AbstractFacade;
use Hurricane\FacadeBundle\Tests\Fixtures\Services\FooService;

/**
 * Class Foo
 * @package App\Tests\Unit\FacadeBundle\Fixtures\Facades
 * @method static callWithArgs($bar, $foo)
 * @method static sayHello()
 */
class Foo extends AbstractFacade
{
    /**
     * {@inheritdoc}
     */
    public static function getFacadeAccessor()
    {
        return FooService::class;
    }
}
