<?php

namespace Hurricane\FacadeBundle\Tests\Fixtures\Facades;

class InvalidFacade
{
    public static function getFacadeAccessor()
    {
        return ['array'];
    }
}
