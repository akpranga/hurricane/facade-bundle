<?php

namespace Hurricane\FacadeBundle\Tests\Fixtures\Services;

class FooService
{
    public function sayHello(): string
    {
        return 'hello';
    }
    public function callWithArgs($bar, $foo): array
    {
        return [$bar, $foo];
    }
}
