<?php

namespace Hurricane\FacadeBundle\Tests;

use Hurricane\FacadeBundle\FacadeBundle;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\ServiceLocator;
use Symfony\Component\HttpKernel\Kernel;

class FacadeBundleTest extends TestCase
{
    public function testBundle()
    {
        $kernel = $this->getKernel();
        $kernel->boot();
        $this->assertTrue($kernel->getContainer()->has('hurricane.facade.container'));
        $this->assertInstanceOf(
            ServiceLocator::class,
            $kernel->getContainer()->get('hurricane.facade.container')
        );
    }
    protected function getKernel()
    {
        $kernel = $this
            ->getMockBuilder(Kernel::class)
            ->setMethods(['registerBundles'])
            ->setConstructorArgs(['test', false])
            ->getMockForAbstractClass()
        ;
        $kernel->method('registerBundles')
            ->willReturn([new FacadeBundle()])
        ;
        $property = new \ReflectionProperty($kernel, 'rootDir');
        $property->setAccessible(true);
        $property->setValue($kernel, sys_get_temp_dir());
        return $kernel;
    }
}
