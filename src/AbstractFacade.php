<?php

namespace Hurricane\FacadeBundle;

use Psr\Container\ContainerInterface;

/**
 * Abstract Class AbstractFacade
 *
 * @package Hurricane\FacadeBundle
 * @author Adrian Pranga <adrian@pranga.com.pl>
 */
abstract class AbstractFacade
{
    /**
     * @var ContainerInterface
     */
    protected static $container;

    /**
     * Facade service container.
     *
     * @param ContainerInterface $container
     */
    public static function setFacadeContainer(ContainerInterface $container)
    {
        static::$container = $container;
    }

    /**
     * Get the registered id of the service.
     *
     * @return string
     */
    abstract protected static function getFacadeAccessor();

    /**
     * Handle dynamic calls to the service.
     *
     * @param string $method
     * @param array $arguments
     *
     * @return mixed
     *
     * @throws \RuntimeException
     */
    public static function __callStatic($method, $arguments)
    {
        $class = \get_called_class();
        if (!static::$container->has($class)) {
            throw new \RuntimeException(sprintf('"%s" facade has not been register.', $class));
        }
        $service = static::$container->get($class);

        return $service->$method(...$arguments);
    }
}
