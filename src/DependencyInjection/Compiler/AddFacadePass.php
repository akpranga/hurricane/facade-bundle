<?php

namespace Hurricane\FacadeBundle\DependencyInjection\Compiler;

use Hurricane\FacadeBundle\AbstractFacade;
use Symfony\Component\DependencyInjection\Alias;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\Compiler\ServiceLocatorTagPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Custom compiler for container
 *
 * @package Hurricane\FacadeBundle\DependencyInjection\Compiler
 * @author Adrian Pranga <adrian@pranga.com.pl>
 */
final class AddFacadePass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        $facades = [];
        foreach ($container->findTaggedServiceIds('hurricane.facade') as $id => $attr) {
            $class = $container->getDefinition($id)->getClass();
            $class = $class ?? $id;
            if (!is_subclass_of($class, AbstractFacade::class)) {
                throw new \InvalidArgumentException(sprintf('The service "%s" must extend AbstractFacade.', $class));
            }
            $r = new \ReflectionMethod($class, 'getFacadeAccessor');
            $r->setAccessible(true);
            $ref = $r->invoke(null);
            if (!\is_string($ref)) {
                throw new \InvalidArgumentException(
                    sprintf(
                        'Facade accessor must be string, "%s" given.',
                        \is_object($ref) ? \get_class($ref) : gettype($ref)
                    )
                );
            }
            $ref = \is_string($ref) && 0 === strpos($ref, '@') ? substr($ref, 1) : $ref;
            $facades[$id] = new Reference($ref);
        }
        $container->setAlias(
            'hurricane.facade.container',
            new Alias(ServiceLocatorTagPass::register($container, $facades), true)
        );
    }
}
