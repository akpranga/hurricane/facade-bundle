<?php

namespace Hurricane\FacadeBundle\DependencyInjection;

use Hurricane\FacadeBundle\AbstractFacade;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

/**
 * FacadeExtension
 *
 * @package Hurricane\FacadeBundle\DependencyInjection
 * @author Adrian Pranga <adrian@pranga.com.pl>
 */
final class FacadeExtension extends Extension
{
    public function load(array $configs, ContainerBuilder $container)
    {
        $container->registerForAutoconfiguration(AbstractFacade::class)
            ->addTag('hurricane.facade');
    }
}
