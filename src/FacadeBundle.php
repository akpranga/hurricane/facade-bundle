<?php

namespace Hurricane\FacadeBundle;

use Hurricane\FacadeBundle\DependencyInjection\Compiler\AddFacadePass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class FacadeBundle
 *
 * @package Hurricane\FacadeBundle
 * @author Adrian Pranga <adrian@pranga.com.pl>
 */
class FacadeBundle extends Bundle
{
    public function boot()
    {
        parent::boot();

        AbstractFacade::setFacadeContainer(
            $this->container->get('hurricane.facade.container')
        );
    }

    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $container->addCompilerPass(new AddFacadePass());
    }
}
